//Code to be able to compute the cost of items I bought including the PA sales tax of 6%
public class Arithmetic{
  //main method required for every Java program
public static void main(String args[]){
  int numPants = 3; //Number of pairs of pants
  double pantsPrice = 34.98; //Cost per pair of pants
  int numShirts = 2; //Number of sweatshirts
  double shirtPrice = 24.99; //Cost per shirt
  int numBelts = 1; //Number of belts
  double beltCost = 33.99; //Cost per belt
  double paSalesTax = 0.06; //the tax rate
  
  double totalCostpant; //total cost of pants
  double totalcostshirt; //total cost of shirts
  double totalcostbelt; //total cost of belts
  double salestaxpant; //sales tax for just pants
  double salestaxshirt; //sales tax for just shirts
  double salestaxbelt; //sales tax for just a belt
  double totalcostpurchase; //the total cost of the purchase without tax
  double totalsalestax; //the total sales tax
  double totalcostpaid; //the total cost paid with sales tax
  
  //entering values for the following data
  totalCostpant=numPants*pantsPrice; //total cost of pants in dollars
  totalcostshirt=numShirts*shirtPrice; //total cost of shirts in dollars
  totalcostbelt=numBelts*beltCost; //total cost of belts in dollars
  salestaxpant=paSalesTax*totalCostpant; //sales tax for total number of pants
  salestaxshirt=paSalesTax*totalcostshirt; //sales tax for total number of shirts purchased
  salestaxbelt=paSalesTax*totalcostbelt; //sales tax for total number of belts purchased
  totalcostpurchase=totalCostpant+totalcostshirt+totalcostbelt; //total cost of the purchase without sales tax
  totalsalestax=salestaxpant+salestaxshirt+salestaxbelt; //total sales tax when buying all the shirts, pants, and belts
  totalcostpaid=totalcostpurchase+totalsalestax; //total cost paid including sales tax
  
  //print out the output data
  System.out.print ("The total cost of pants is $");
  System.out.printf("%.2f", totalCostpant); 
  System.out.println();
  System.out.println ("The total cost of shirts is $"+totalcostshirt+"");
  System.out.println ("The total cost of belts is $"+totalcostbelt+"");
  System.out.print("The total sales tax for pants is $");
  System.out.printf("%.2f", salestaxpant); 
  System.out.println();
  System.out.print("The total sales tax for shirts is $");
  System.out.printf("%.2f", salestaxshirt); 
  System.out.println();
  System.out.print("The total sales tax for shirts is $");
  System.out.printf("%.2f", salestaxbelt); 
  System.out.println();
  System.out.println ("The total cost of the purchase is $"+totalcostpurchase+"");
  System.out.print("The total sales tax is $");
  System.out.printf("%.2f", totalsalestax); 
  System.out.println();
  System.out.print("The total cost paid is $");
  System.out.printf("%.2f", totalcostpaid); 
  System.out.println();
  
} //end of main method
} //end of class