//hw07, 03/25/19, Noah Sutherland
//this homework section is supposed to help give me practice writing methods, overloading methods, and forcing the user to enter good Input
//this specific program can process a string by examining all the characters, and determining if they are letters
//the program will then let the user enter a string and choose if he or she wants to examine all the characters or just a certain number
import java.util.Scanner;
public class StringAnalysis{
  public static void ExamineCharacters(String UserInput, int UserNumber){ //first method
    for (int i = 0; i < UserNumber && i < UserInput.length(); i++){
      char UserWord = UserInput.charAt(i); //initializing UserWord as a character
      if (Character.isLetter(UserWord)){ //if statement for if the characters in the first user input are letters
        System.out.println(UserWord + " is a letter!"); //prints what the user typed in as their character and that it is a letter!
      }
      else { //else statement for if the characters in the first user input are not letters
        System.out.println(UserWord + " is not a letter!"); //prints what the user typed in as their character and that it is not a letter!
      }  }  }

  public static void ExamineCharacters(String UserInput){ //second method
    for (int i = 0; i < UserInput.length(); i++){
      char UserWord = UserInput.charAt(i);
      if (Character.isLetter(UserWord)){ //if statement for if the characters in the first user input are letters
        System.out.println(UserWord + " is a letter!"); //prints what the user typed in as their character and that it is a letter!
      }
      else { //else statement for if the characters in the first user input are not letters
        System.out.println(UserWord + " is not a letter!"); //prints what the user typed in as their character and that it is not a letter!
      }    }  }

  public static void main(String[] args){ //third method and is the main method
    Scanner myScanner=new Scanner(System.in); //assigning the scanner to the name myScanner
    System.out.println("Please type out a single string: "); //prints out asking the user to type a string
    String UserInput=myScanner.next();
    int i=0; //initializing the variable i as an integer
    while (i < 1){ //a while loop for when the integer i is greater than 1
      System.out.println("Please type in a positive integer for what amount of your string you want to examine, or the string [All] if you want to examine all of your string input: "); //asks the user to type in either a positive integer for how many characters they want to examine or the string all to examine all of the characters and whether they are letters or not
      boolean UserCorrectness=myScanner.hasNextInt(); //initializing UserCorrectness as a boolean
      if (UserCorrectness == true){ //if statement for if the UserCorrectness turns out to be true
        int UserNumber=myScanner.nextInt(); //initializing UserNumber as an integer
        if (UserNumber<0){ //if statement for if the UserNumber is less than 0
        }
        else { //else statement for if the UserNumber is greater than 0
          ExamineCharacters(UserInput, UserNumber);
          i++; //incrementing i
        }      }
      else {
        String UserInputForAll=myScanner.next(); //initializing UserInputForAll as a string and equal to the next string input from the user
        if (UserInputForAll.equals("All")) { //an if statement for if the UserInputForAll equals all
          i++; //incrementing i
          ExamineCharacters(UserInput);
        }
        else {
        }      }    }  }

} //end of public class
