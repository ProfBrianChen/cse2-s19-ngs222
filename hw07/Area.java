//hw07, 03/25/19, Noah Sutherland
//this homework section is supposed to help give me practice writing methods, overloading methods, and forcing the user to enter good input
//this program specifically will ask the user for three different shapes, then for the dimensions of the shape, then will output that shapes Area
import java.lang.Math;
import java.util.Random;
import java.util.Scanner;
public class Area{
  public static double AreaofRectangle(double Length, double Width){ //method created for the area of the rectangle
    double Area = Length*Width; //the equation that gets the area of the rectangle
    return Area;
  }
  public static double AreaofTriangle(double Base, double Height){ //method created for the area of the triangle
    double Area = Base*Height/2; //the equation that gets the area of the triangle
    return Area;
  }
  public static double AreaofCircle(double Radius){ //method created for the area of the circle
    double Area = Math.PI*Math.pow(Radius,2); //the equation that gets the area of the circler
    return Area;
  }

  public static void main(String[] args){ //main method for all of the inputs and prints
    Scanner myScanner = new Scanner(System.in); //initializing scanner and calling it myScanner
    System.out.println("Which of the following shapes is your choice? (rectangle, triangle, circle)"); //asks the user for which shape is their choice
    String UserChoice = myScanner.next();
    if (UserChoice.equals("rectangle")){ //if statement for if the user chooses a rectangle
      System.out.println("Please type in the rectangle length as a double: "); //asks the user to input the length of the rectangle if they input a rectangle
      double LengthofRectangle = myScanner.nextDouble();
      System.out.println("Please type in the rectangle width as a double: "); //asks the user to input the width of the rectangle if they input rectangle
      double WidthofRectangle = myScanner.nextDouble();
      System.out.println("The area of the rectangle is: "); //prints the area of the rectangle is
      System.out.println(AreaofRectangle(LengthofRectangle,WidthofRectangle)); //prints the calculated area of the rectangle
    }

    else if (UserChoice.equals("triangle")){ //else if statement for if the user chose a traingle
      System.out.println("Please type in the triangle base as a double: "); //asks the user to input the base of the triangle
      double BaseofTriangle = myScanner.nextDouble();
      System.out.println("Please type in the triangle height as a double: "); //asks the user to input the height of the triangle
      double HeightofTriangle = myScanner.nextDouble();
      System.out.println("The area of the triangle is: "); //prints the area of the triangle is
      System.out.println(AreaofTriangle(BaseofTriangle,HeightofTriangle)); //prints the calculated area of the triangle
    }

    else if (UserChoice.equals("circle")){ //else if statement for if the user chose a circle
      System.out.println("Please type in the circle radius as a double: "); //asks the user to input the radius of the circle
      double RadiusofCircle = myScanner.nextDouble();
      System.out.println("The area of the circle is: "); //prints the area of the circle is
      System.out.println(AreaofCircle(RadiusofCircle)); //prints the calculated area of the circle
  }

    else { //an else statement for if the user did not type in any of the three options
      System.out.println("You did not type in any of the shapes provided. Please enter a shape provided: "); //asks the user to enter one of the correct shapes provided
      while(true){
         System.out.println("Which of the following shapes is your choice? (triangle, rectangle, circle)"); //asks the user for which shape is their choice
         UserChoice = myScanner.next();
         if (UserChoice.equals("rectangle")){
        System.out.println("Please type in the rectangle length as a double: "); //asks the user to input the length of the rectangle
        double LengthofRectangle = myScanner.nextDouble();
        System.out.println("Please type in the rectangle width as a double: "); //asks the user to input the width of the rectangle
        double WidthofRectangle = myScanner.nextDouble();
        System.out.println("The area of the rectangle is: "); //prints the area of the rectangle is
        System.out.println(AreaofRectangle(LengthofRectangle,WidthofRectangle)); //prints the calculated area of the rectangle
        break; //breaks out of the loop
      }

      else if (UserChoice.equals("triangle")){ //an else if statement for if the input chosen was a triangle
        System.out.println("Please type in the triangle base as a double: "); //asks the user to input the base of the triangle
        double BaseofTriangle = myScanner.nextDouble();
        System.out.println("Please type in the triangle height as a double: "); //asks the user to input the height of the triangle
        double HeightofTriangle = myScanner.nextDouble();
        System.out.println("The area of the triangle is: "); //prints the area of the triangle is
        System.out.println(AreaofTriangle(BaseofTriangle,HeightofTriangle)); //prints the calculated area of the triangle
        break; //breaks out of the loop
      }

      else if (UserChoice.equals("circle")){ //an else if statement for if the input chosen was a circle
        System.out.println("Please type in the circle radius as a double: "); //asks the user to input the radius of the circle
        double RadiusofCircle = myScanner.nextDouble();
        System.out.println("The area of the circle is: "); //prints the area of the circle is
        System.out.println(AreaofCircle(RadiusofCircle)); //prints the calculated area of the circle
        break; //breaks out of the loop
}    }   }

  }
}//end of the public class