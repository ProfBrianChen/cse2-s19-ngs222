//Noah Sutherland, 04/29/19, gives practice working on two dimensional arrays
import java.util.Scanner;
import java.util.Arrays;
public class Straight{
  public static int[][] deckShuffler(){ //first method that shuffles the deck
   int[][] deckShuffler=new int[4][13]; //is the 2D array in which the first one is 4 for the suit and the second is size 13 in which is the actual number
    int x=0; //initializes x to be equal 0
    for(int i=0; i<4; i++){ //for loop for the suit that sets i equal to 0 and runs when i is less than 4 and increments i by 1
      for(int k=0; k<13; k++){ //for loop for the card number that sets k equal to 0 and runs when k is less than 13 and increments k by 1
        deckShuffler[i][k]=x; //sets the deck shuffled two dimensional array of suit i and card k equal to x
        x++; //increments x by 1
      }  }
    int rowFinder=0; //initializes rowFinder to be equal to 0
    int columnFinder=0; //initializes columnFinder to be equal to 0
    int numberSwap=0; //initializes numberSwap to be equal to 0
    for(int i=0; i<4; i++){ //for loop for suit that initializes i equal to 0 and runs when i is less than 4 and increments i by 1
      for(int k=0; k<13; k++){ //for loop for card number that initializes k equal to 0 and runs when k is less than 13 and increments k by 1
           rowFinder=(int)(Math.random()*4); //has rowFinder equal to Math.random multiplied by 4
           columnFinder=(int)(Math.random()*13); //makes columnFinder equal to Math.random multiplied by 13
           numberSwap=deckShuffler[i][k]; //makes number swap equal to the deckShuffler 2 dimensional array of the random suit and card number
           deckShuffler[i][k]=deckShuffler[rowFinder][columnFinder]; //makes deckShuffler of the random suit and card number equal to the rowFinder and columnFinder that were just identified
           deckShuffler[rowFinder][columnFinder]=numberSwap; //makes the number given above equal to the numberSwap
      }    }
    return deckShuffler; //returns the 2 dimensional array
  }

  public static int[] firstFive(int[][] array){
    int[] firstFive=new int[5]; //initializes the single dimensional array called firstFive
    for(int i=0; i<5; i++){ //for loop that has i equal to 0 and runs when i is less than 5 and increments i by 1
      firstFive[i]=array[0][i]; //the first five single dimensional array at number i is equal to the two dimensional array put in at suit 0 and the number i
    }
     return firstFive; //return the single dimensional array firstFive
  }

  public static int arraySearch(int[] array, int k){
    if(k<=0 || k>5){ //if statement that runs when k is less than or equal to 0 or is greater than 5 to test for errors
      System.out.println("There was an error."); //prints "There was an error."
      return 0; //will return 0 if there was an error
    }
    int[] newArray=new int[5]; //initializes the single dimensional array of newArray
    for(int i=0; i<5; i++){ //for loop that has i equal to 0 and runs when i is less than 5 and increments i by 1
      newArray[i]=(array[i]%13)+1; //makes newArray at number i equal to the input array at number i and its remainder divided by 13 plus 1
    }
    int numberSwap=0; //makes the integer numberSwap equal to 0
    for(int x=0; x<5; x++){ //for loop that has x equal to 0 and runs when x is less than 5 and increments x by 1
     for(int y=1; x+y<5; y++){ //for loop that has y equal to 1 and runs when x plus y is less than 5 and increments y by 1
      if(newArray[x+y]<newArray[x]){ //if statement for if the newArray of x plus y is less than the newArray of just x
        numberSwap=newArray[x]; //makes the numberSwap equal to the newArray at x
        newArray[x]=newArray[x+y]; //makes the newArray at x now equal to the newArray at x plus y
        newArray[x+y]=numberSwap; //and finally ends off with the newArray at x plus y equal to the numberSwap
      }    }   }
    return newArray[k-1]; //this will in turn return the newArray at integer k minus 1
  }

  public static boolean arraySearch(int[] array){
    for(int i=1; i<array.length; i++){ //for loop that has i equal to 1 and runs when i is less than the array length and increments i by 1
      if(arraySearch(array, i+1)-arraySearch(array, i)!=1){ //if statement for when the arraySearch of number array and i plus 1 minus the arraySearch of array and i does not equal to 1
        return false; //will return false
      }  }
    return true; //otherwise it returns true
  }

  public static void main(String args[]){
    double numStraights=0; //initializes the double of numStraights equal to 0 at the start
    for(int i=0; i<1000000; i++){ //for loop that will run 1000000 times
    int[][] array=deckShuffler(); //makes the two dimensional array called array equal to the output of the deckShuffler
    int[] myHand=firstFive(array); //makes the one dimensional array called myHand equal to the output of the method firstFive with array
    boolean straightCheck=arraySearch(myHand); //makes the boolean of the straightCheck equal to the output of the method arraySearch with myHand
    if(straightCheck){ //if statement for if it is a straight
      numStraights++; //will add one to the numStraights (number of straights)
    }  }
    double probStraight=(float)(numStraights/10000); //initializes and makes the probability of a straight equal to the calculation of the probability of a straight after checked 1000000 times
    System.out.printf("The probability is: "+"%.4f%n", probStraight); //will print "The probability is: "and then print the probability to four decimal places
  }
} //end of public class
