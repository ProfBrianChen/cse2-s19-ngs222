//Noah Sutherland, 04/29/19, and produces a city with numbers between 100-999 with 7 different segments
import java.lang.Math;
import java.util.Random;
public class RobotCity{
   public static int[][] BuildArray(){
        Random randomNum=new Random(); //initiates randomNum to generate a random number
        int rowNum=randomNum.nextInt(5)+10; //makes the row number a random number between 10-15
        int columnNum=randomNum.nextInt(5)+10; //makes the column number equal to a random number between 10-15
        int[][] array=new int[columnNum][rowNum]; //creates a 2 dimensional array that has pointers as the column number and row number
        for(int x=0; x<array.length; x++){ //for loop that initializes x as 0 and runs when x is less than the array length and increments x by 1
            for(int y=0; y<array[x].length; y++){ //for loop that has y equal to 0 and runs when y is less than the array length of pointer x and increments y by 1
                array[x][y]=(int)((Math.random()*(999-100))+100); //makes the array of x and y equal to a random number between 100-999 for the numbers in each column and row
            }    }
      return array; //returns the array
    }

    public static void Display(int[][] array){
        for (int x=array[0].length-1; x>=0; x--){ //for loop that has x equal to the array length at 0 minus 1 and runs when x is greater than or equal to 0 and minuses x by 1
            System.out.print("{"); //prints out the start of the row
            for (int y=0; y<array.length; y++){ //for loop that has y equal to 0 and runs when y is less than the array length and increments y by 1
                System.out.printf("%4d", array[y][x]); //prints out the random number
                System.out.print(" "); //prints out a space
            }
            System.out.println("}"); //prints out the end of the row
        }
        System.out.println(""); //enters the line
    }

    public static void Invade(int[][] array, int k){
        Random randomNum=new Random(); //initializes randomNum
        for(int x=0; x<k; x++){ //for loop that has x equal to 0 and runs when x is less than k and increments x by 1
        int rowNum=randomNum.nextInt(array[0].length);
        int columnNum=randomNum.nextInt(array.length);
            if (array[columnNum][rowNum]>0){ //if statement that runs when the two dimensional array pointer from column number and row number is greater than 0
                array[columnNum][rowNum]=-1*array[columnNum][rowNum]; //makes that number equal to itself but negative
            }      }    }

    public static void Update(int[][] array){
        for (int x=0; x<array[0].length; x++){ //for loop that has x equal to 0 and runs when x is less than the array length of pointer 0 and increments x by 1
            for (int y=array.length-1; y>=0; y--){ //has integer y equal to the array length minus 1 and runs when y is greater than or equal to 0 and minuses y by 1
                if(array[y][x]<0){ //if statement for if array of pointer y and x is less than 0
                    if(y==array.length-1){ //if statement for if y is equal to the array length minus 1
                        array[y][x]=-1*array[y][x]; //makes the original array equal to itself but negative
                    }
                    else{
                        array[y][x]=-1*array[y][x]; //makes the original array equal to itself but negative
                        array[y+1][x]*=-1; //takes that array and moves it over and multiplies it by -1
                        y++; //increments y by 1
                    }        }        }      }    }

    public static void main(String args[]){
        int[][] array=BuildArray(); //makes the two dimensional array equal to the built array we have in the first method
        Random randomNum=new Random(); //makes randomNum equal to a new random
        Display(array); //goes into display with array
        Invade(array, randomNum.nextInt(10)+1);
        Display(array); //goes into display with array for the second time
        for (int x=0; x<5; x++){ //for loop that has x equal to 0 and runs when x is less than 5 and increments x by 1
            Update(array); //puts array in the update method
            Display(array); //puts array in the display method
        }    }
} //end of public class
