//this is a homework that gives us practice writing loops. 
//this program asks a user for different academic information including the course number, department name, number of times it meets in a week
//the time the class starts, the instructor name, and the number of students.
//the program purpose is to check and make sure they input the right thing, whether that be an integer or a loop.the

import java.util.Scanner; //importing the Scanner that will let the program prompt the user
public class Hw05 { //main method used for every java program
  public static void main (String[] args) {
    Scanner myScanner = new Scanner (System.in); 
    
    //while loop for course number
    System.out.println("Input your course number: "); //asking the user for their course number
    while (true) {
      boolean correct = myScanner.hasNextInt();
      if (correct) {
      int num1 = myScanner.nextInt(); //assigning a variable to the integer the user has input
      System.out.println("Your course number is: " +num1);
      break;
    }
    else {
      System.out.print("Please enter the correct course number integer: "); //if the user input incorrectly than it is asking them for the correct response
      myScanner.next();
    }
    }
    
    //while loop for the department name
     System.out.println("Input the department name: "); //asking the user for their department name
    while (true) {
      boolean correct = myScanner.hasNext();
      if (correct) {
      String name1 = myScanner.next(); //assigning a variable to what the user has input
      System.out.println("The department name is: " +name1);
      break;
    }
    else {
      System.out.print("Please enter the correct department name in a String: "); //if the user input incorrectly than it is asking them for the correct response
      myScanner.next();
    }
    }
    
    //while loop for the number of times it meets a week
     System.out.println("Input the number of times you meet per week: "); //asking the user for the number of times their class meets per week
    while (true) {
      boolean correct = myScanner.hasNextInt();
      if (correct) {
      int num2 = myScanner.nextInt(); //assigning a variable to what the user input
      System.out.println("The number of times your course meets per week is: " +num2);
      break;
    }
    else {
      System.out.print("Please enter the correct amount of times you meet per week as an integer: "); //if the user input incorrectly than it is asking them for the correct response
      myScanner.next();
    }
    }
    
    //while loop for the time the class starts
     System.out.println("Input the time your class starts: "); //asking the user for the time their class starts
    while (true) {
      boolean correct = myScanner.hasNext();
      if (correct) {
      String name3 = myScanner.next(); //assigning a variable to what the user input
      System.out.println("The time your class starts is: " +name3);
      break;
    }
    else {
      System.out.print("Please enter the correct time your class starts: "); //if the user input incorrectly than it is asking them for the correct response
      myScanner.next();
    }
    }
    
    //while loop for the instructor name
    System.out.println("Input the name of your instructor: "); //asking the user for the name of their instructor
    while (true) {
      boolean correct = myScanner.hasNext();
      if (correct) {
      String name2 = myScanner.next(); //assigning a variable to what the user input
      System.out.println("Your instructor's name is: " +name2);
      break;
    }
    else {
      System.out.print("Please enter the correct name for your instructor in a String: "); //if the user input incorrectly than it is asking them for the correct response
      myScanner.next();
    }
    }
    
    //while loop for the number of students
     System.out.println("Input the total number of students: "); //asking the user for the total number of students in their class
    while (true) {
      boolean correct = myScanner.hasNextInt();
      if (correct) {
      int num4 = myScanner.nextInt(); //assigning a variable to what the user input
      System.out.println("The total number of students is: " +num4);
      break;
    }
    else {
      System.out.print("Please enter the correct total number of students as in integer: "); //if the user input incorrectly than it is asking them for the correct response
      myScanner.next();
    }
    }
   
        
  }//end of main method
}//end of public class