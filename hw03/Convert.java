//this program converts meters to inches when asking the user for an answer in meters
import java.util.Scanner;
public class Convert{
  //main method required for every java program
  public static void main (String args []) {
    Scanner myScanner = new Scanner ( System.in ); //assigning a specific name to the Scanner
    System.out.print("Enter the distance in meters: "); //printing out the "Enter the distance in meters:" for the user to input data
    double meters = myScanner.nextDouble(); //creating so the user can input their data
    double inches = meters*39.370078878; //assigning a way to convert meters to inches
    System.out.print(meters + " meters is ");
    System.out.printf("%.4f", inches); //making sure that the decimal places only go to 4
    System.out.println(" inches.");
    
  } //end of main method
} //end of class
      