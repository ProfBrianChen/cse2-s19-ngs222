// creating a random number generator that generates five different numbers to create a specific poker hand (this took so long and was very very very hard) 
import java.lang.Math;
public class PokerHandCheck{
  //main method required for every Java program
  public static void main(String[] args){
    
    System.out.println("Your random cards were: ");
    int rand_number1; //making the random number an integer
    int card_number1; //making the card number an integer
    int true_card_number1; //making the true card number an integer
   
    //counts how many times a certain card occurs so I can calculate how many pais or triple repeats there are for a number
    int count1 =0; 
    int count2 =0; 
    int count3 =0; 
    int count4 =0;
    int count5 =0;
    int count6 =0;
    int count7 =0;
    int count8 =0;
    int count9 =0;
    int count10 =0;
    int count11 =0;
    int count12 =0;
    int count0 =0;
    
    float suit1;
    rand_number1 = (int)(Math.random()*52+1); //assinging random numbers to be from 1-52
    card_number1= (rand_number1+13) % 13 ; //assigning the card number range to be 13
    true_card_number1 = card_number1; //having the true card number equal the random card number that is drawn
    suit1 = rand_number1 / 13 ; //making sure that each suit only ranges between 13
   
    //System.out.println("The random number is " +rand_number); //prints out random number is and then what the random number generator generates
    
    //for card 1 
    if (suit1 == 0) {
      if (card_number1 == 1) { 
        System.out.println("the ace of diamonds"); //printing Ace when the random number is 1
        count1++;
      }
      else if (card_number1 == 11) {
       System.out.println("the jack of diamonds "); //printing Jack when the random number is 11
        count11++;
      }
      else if (card_number1 == 12) {
       System.out.println(" the Queen of Diamonds "); //printing Queen when the random number is 12
        count12++;
      }
      else if (card_number1 == 0) {
       System.out.println(" the King of Diamonds "); //printing King when the random number is 13
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number1  + " of Diamonds "); //printing whatever the random number is (other than 1, 11, 12, and 13)
      }}
   
    else if (suit1 == 1) { //making sure that all numbers below that will be between 14-26 are clubs a.k.a suit == 1
      if (card_number1 == 1) {
        System.out.println(" the Ace of Clubs"); //printing Ace when the random number is 14
        count1++;
      }
    else if (card_number1 == 11) {
       System.out.println(" the Jack of Clubs "); //printing Jack when the random number is 24
      count11++;
      }
      else if (card_number1 == 12) {
       System.out.println(" the Queen of Clubs "); //printing Queen when the random number is 25
        count12++;
      }
      else if (card_number1 == 0) {
       System.out.println(" the King of Clubs "); //printing King when the random number is 26
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number1  + " of Clubs "); //printing whatever the random number is (other than 14, 24, 25, and 26)
      }}
    
    else if (suit1 == 2) { //making sure that all numbers below that will be between 27-39 are hearts a.k.a suit == 2
      if (card_number1 == 1) {
        System.out.println(" the Ace of Hearts"); //printing Ace when the random number is 27
        count1++;
      }
    else if (card_number1 == 11) {
       System.out.println(" the Jack of Hearts "); //printing Jack when the random number is 37
      count11++;
      }
      else if (card_number1 == 12) {
       System.out.println(" the Queen of Hearts "); //printing Queen when the random number is 38
        count12++;
      }
      else if (card_number1 == 0) {
       System.out.println(" the King of Hearts "); //printing King when the random number is 39
       count0++;
      }
      else {
       System.out.println(" the " + true_card_number1  + " of Hearts "); //printing whatever the random number is (other than 27. 37, 38, and 39)
      }}
    
    else if (suit1 == 3) { //making sure that all numbers below that will be between 40-52 are spades a.k.a suit ==3
     if (card_number1 == 1) {
        System.out.println(" the Ace of Spades"); //printing Ace when the random number is 40
       count1++;
      }
    else if (card_number1 == 11) {
       System.out.println(" the Jack of Spades "); //printing Jack when the random number is 50
      count11++;
      }
      else if (card_number1 == 12) {
       System.out.println(" the Queen of Spades "); //printing Queen when the random number is 51
        count12++;
      }
      else if (card_number1 == 0) {
       System.out.println(" the King of Spades "); //printing King when the random number is 52
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number1  + " of Spades "); //printing whatever the random number is (other than 40, 50, 51, and 52)
      }}
    //counts how many times these numbers occur in card 1
    if (true_card_number1 == 2){ 
      count2++;
    }
    else if (true_card_number1 == 3){ 
      count3++;
    }
    else if (true_card_number1 == 4){
      count4++;
    }
    else if (true_card_number1 ==5){
      count5++;
    }
    else if (true_card_number1 ==6){
      count6++;
    }
    else if (true_card_number1 ==7){
      count7++;
    }
    else if (true_card_number1 ==8){
      count8++;
    }
    else if (true_card_number1 ==9){
      count9++;
    }
    else if (true_card_number1 ==10){
      count10++;
    }
    
    //for card 2 
    int rand_number2; //making the random number an integer
    int card_number2; //making the card number an integer
    int true_card_number2; //making the true card number an integer
    float suit2;
    rand_number2 = (int)(Math.random()*52+1); //assinging random numbers to be from 1-52
    card_number2= (rand_number2 +13) % 13 ; //assigning the card number range to be 13
    true_card_number2 = card_number2; //having the true card number equal the random card number that is drawn
    suit2 = rand_number2 / 13 ; //making sure that each suit only ranges between 13
   
    //System.out.println("The random number is " +rand_number); //prints out random number is and then what the random number generator generates
    
    if (suit2 == 0) {
      if (card_number2 == 1) {
        System.out.println(" the Ace of Diamonds"); //printing Ace when the random number is 1
        count1++;
      }
      else if (card_number2 == 11) {
       System.out.println(" the Jack of Diamonds "); //printing Jack when the random number is 11
        count11++;
      }
      else if (card_number2 == 12) {
       System.out.println(" the Queen of Diamonds "); //printing Queen when the random number is 12
        count12++;
      }
      else if (card_number2 == 0) {
       System.out.println(" the King of Diamonds "); //printing King when the random number is 13
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number2  + " of Diamonds "); //printing whatever the random number is (other than 1, 11, 12, and 13)
      }}
   
    else if (suit2 == 1) { //making sure that all numbers below that will be between 14-26 are clubs a.k.a suit == 1
      if (card_number2 == 1) {
        System.out.println(" the Ace of Clubs"); //printing Ace when the random number is 14
        count1++;
      }
    else if (card_number2 == 11) {
       System.out.println(" the Jack of Clubs "); //printing Jack when the random number is 24
      count11++;
      }
      else if (card_number2 == 12) {
       System.out.println(" the Queen of Clubs "); //printing Queen when the random number is 25
        count12++;
      }
      else if (card_number2 == 0) {
       System.out.println(" the King of Clubs "); //printing King when the random number is 26
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number2  + " of Clubs "); //printing whatever the random number is (other than 14, 24, 25, and 26)
      }}
    
    else if (suit2 == 2) { //making sure that all numbers below that will be between 27-39 are hearts a.k.a suit == 2
      if (card_number2 == 1) {
        System.out.println(" the Ace of Hearts"); //printing Ace when the random number is 27
        count1++;
      }
    else if (card_number2 == 11) {
       System.out.println(" the Jack of Hearts "); //printing Jack when the random number is 37
      count11++;
      }
      else if (card_number2 == 12) {
       System.out.println(" the Queen of Hearts "); //printing Queen when the random number is 38
        count12++;
      }
      else if (card_number2 == 0) {
       System.out.println(" the King of Hearts "); //printing King when the random number is 39
       count0++;
      }
      else {
       System.out.println(" the " + true_card_number2  + " of Hearts "); //printing whatever the random number is (other than 27. 37, 38, and 39)
      }}
    
    else if (suit2 == 3) { //making sure that all numbers below that will be between 40-52 are spades a.k.a suit ==3
     if (card_number2 == 1) {
        System.out.println(" the Ace of Spades"); //printing Ace when the random number is 40
       count1++;
      }
    else if (card_number2 == 11) {
       System.out.println(" the Jack of Spades "); //printing Jack when the random number is 50
      count11++;
      }
      else if (card_number2 == 12) {
       System.out.println(" the Queen of Spades "); //printing Queen when the random number is 51
        count12++;
      }
      else if (card_number2 == 0) {
       System.out.println(" the King of Spades "); //printing King when the random number is 52
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number2  + " of Spades "); //printing whatever the random number is (other than 40, 50, 51, and 52)
      }}
      if (true_card_number2 == 2){ 
      count2++;
        //counts how many times these numbers occur in card 2
    }
    else if (true_card_number2 == 3){ 
      count3++;
    }
    else if (true_card_number2 == 4){
      count4++;
    }
    else if (true_card_number2 ==5){
      count5++;
    }
    else if (true_card_number2 ==6){
      count6++;
    }
    else if (true_card_number2 ==7){
      count7++;
    }
    else if (true_card_number2 ==8){
      count8++;
    }
    else if (true_card_number2 ==9){
      count9++;
    }
    else if (true_card_number2 ==10){
      count10++;
    }
    
    // for card 3
    int rand_number3; //making the random number an integer
    int card_number3; //making the card number an integer
    int true_card_number3; //making the true card number an integer
    float suit3;
    rand_number3 = (int)(Math.random()*52+1); //assinging random numbers to be from 1-52
    card_number3= (rand_number3 +13) % 13 ; //assigning the card number range to be 13
    true_card_number3 = card_number3; //having the true card number equal the random card number that is drawn
    suit3 = rand_number3 / 13 ; //making sure that each suit only ranges between 13
   
    //System.out.println("The random number is " +rand_number); //prints out random number is and then what the random number generator generates
    
    if (suit3 == 0) {
      if (card_number3 == 1) {
        System.out.println(" the Ace of Diamonds"); //printing Ace when the random number is 1
        count1++;
      }
      else if (card_number3 == 11) {
       System.out.println(" the Jack of Diamonds "); //printing Jack when the random number is 11
        count11++;
      }
      else if (card_number3 == 12) {
       System.out.println(" the Queen of Diamonds "); //printing Queen when the random number is 12
        count12++;
      }
      else if (card_number3 == 0) {
       System.out.println(" the King of Diamonds "); //printing King when the random number is 13
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number3  + " of Diamonds "); //printing whatever the random number is (other than 1, 11, 12, and 13)
      }}
   
    else if (suit3 == 1) { //making sure that all numbers below that will be between 14-26 are clubs a.k.a suit == 1
      if (card_number3 == 1) {
        System.out.println(" the Ace of Clubs"); //printing Ace when the random number is 14
        count1++;
      }
    else if (card_number3 == 11) {
       System.out.println(" the Jack of Clubs "); //printing Jack when the random number is 24
      count11++;
      }
      else if (card_number3 == 12) {
       System.out.println(" the Queen of Clubs "); //printing Queen when the random number is 25
       count12++;
      }
      else if (card_number3 == 0) {
       System.out.println(" the King of Clubs "); //printing King when the random number is 26
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number3  + " of Clubs "); //printing whatever the random number is (other than 14, 24, 25, and 26)
      }}
    
    else if (suit3 == 2) { //making sure that all numbers below that will be between 27-39 are hearts a.k.a suit == 2
      if (card_number3 == 1) {
        System.out.println(" the Ace of Hearts"); //printing Ace when the random number is 27
        count1++;
      }
    else if (card_number3 == 11) {
       System.out.println(" the Jack of Hearts "); //printing Jack when the random number is 37
      count11++;
      }
      else if (card_number3 == 12) {
       System.out.println(" the Queen of Hearts "); //printing Queen when the random number is 38
        count12++;
      }
      else if (card_number3 == 0) {
       System.out.println(" the King of Hearts "); //printing King when the random number is 39
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number3  + " of Hearts "); //printing whatever the random number is (other than 27. 37, 38, and 39)
      }}
    
    else if (suit3 == 3) { //making sure that all numbers below that will be between 40-52 are spades a.k.a suit ==3
     if (card_number3 == 1) {
        System.out.println(" the Ace of Spades"); //printing Ace when the random number is 40
       count1++;
      }
    else if (card_number3 == 11) {
       System.out.println(" the Jack of Spades "); //printing Jack when the random number is 50
      count11++;
      }
      else if (card_number3 == 12) {
       System.out.println(" the Queen of Spades "); //printing Queen when the random number is 51
        count12++;
      }
      else if (card_number3 == 0) {
       System.out.println(" the King of Spades "); //printing King when the random number is 52
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number3  + " of Spades "); //printing whatever the random number is (other than 40, 50, 51, and 52)
      }}
    //counts how many times these numbers occur in card 3
      if (true_card_number3 == 2){ 
      count2++;
    }
    else if (true_card_number3 == 3){ 
      count3++;
    }
    else if (true_card_number3 == 4){
      count4++;
    }
    else if (true_card_number3 ==5){
      count5++;
    }
    else if (true_card_number3 ==6){
      count6++;
    }
    else if (true_card_number3 ==7){
      count7++;
    }
    else if (true_card_number3 ==8){
      count8++;
    }
    else if (true_card_number3 ==9){
      count9++;
    }
    else if (true_card_number3 ==10){
      count10++;
    }
    
    //for card 4
    int rand_number4; //making the random number an integer
    int card_number4; //making the card number an integer
    int true_card_number4; //making the true card number an integer
    float suit4;
    rand_number4 = (int)(Math.random()*52+1); //assinging random numbers to be from 1-52
    card_number4= (rand_number4 +13) % 13 ; //assigning the card number range to be 13
    true_card_number4 = card_number4; //having the true card number equal the random card number that is drawn
    suit4 = rand_number4 / 13 ; //making sure that each suit only ranges between 13
   
    //System.out.println("The random number is " +rand_number); //prints out random number is and then what the random number generator generates
    
    if (suit4 == 0) {
      if (card_number4 == 1) {
        System.out.println(" the Ace of Diamonds"); //printing Ace when the random number is 1
        count1++;
      }
      else if (card_number4 == 11) {
       System.out.println(" the Jack of Diamonds "); //printing Jack when the random number is 11
        count11++;
      }
      else if (card_number4 == 12) {
       System.out.println(" the Queen of Diamonds "); //printing Queen when the random number is 12
        count12++;
      }
      else if (card_number4 == 0) {
       System.out.println(" the King of Diamonds "); //printing King when the random number is 13
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number4  + " of Diamonds "); //printing whatever the random number is (other than 1, 11, 12, and 13)
      }}
   
    else if (suit4 == 1) { //making sure that all numbers below that will be between 14-26 are clubs a.k.a suit == 1
      if (card_number4 == 1) {
        System.out.println(" the Ace of Clubs"); //printing Ace when the random number is 14
        count1++;
      }
    else if (card_number4 == 11) {
       System.out.println(" the Jack of Clubs "); //printing Jack when the random number is 24
      count11++;
      }
      else if (card_number4 == 12) {
       System.out.println(" the Queen of Clubs "); //printing Queen when the random number is 25
        count12++;
      }
      else if (card_number4 == 0) {
       System.out.println(" the King of Clubs "); //printing King when the random number is 26
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number4  + " of Clubs "); //printing whatever the random number is (other than 14, 24, 25, and 26)
      }}
    
    else if (suit4 == 2) { //making sure that all numbers below that will be between 27-39 are hearts a.k.a suit == 2
      if (card_number4 == 1) {
        System.out.println(" the Ace of Hearts"); //printing Ace when the random number is 27
        count1++;
      }
    else if (card_number4 == 11) {
       System.out.println(" the Jack of Hearts "); //printing Jack when the random number is 37
      count11++;
      }
      else if (card_number4 == 12) {
       System.out.println(" the Queen of Hearts "); //printing Queen when the random number is 38
        count12++;
      }
      else if (card_number4 == 0) {
       System.out.println(" the King of Hearts "); //printing King when the random number is 39
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number4  + " of Hearts "); //printing whatever the random number is (other than 27. 37, 38, and 39)
      }}
    
    else if (suit4 == 3) { //making sure that all numbers below that will be between 40-52 are spades a.k.a suit ==3
     if (card_number4 == 1) {
        System.out.println(" the Ace of Spades"); //printing Ace when the random number is 40
       count1++;
      }
    else if (card_number4 == 11) {
       System.out.println(" the Jack of Spades "); //printing Jack when the random number is 50
      count11++;
      }
      else if (card_number4 == 12) {
       System.out.println(" the Queen of Spades "); //printing Queen when the random number is 51
        count12++;
      }
      else if (card_number4 == 0) {
       System.out.println(" the King of Spades "); //printing King when the random number is 52
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number4  + " of Spades "); //printing whatever the random number is (other than 40, 50, 51, and 52)
      }}
      if (true_card_number4 == 2){ 
      count2++;
     //counts how many times these numbers occur in card 4
    }
    else if (true_card_number4 == 3){ 
      count3++;
    }
    else if (true_card_number4 == 4){
      count4++;
    }
    else if (true_card_number4 ==5){
      count5++;
    }
    else if (true_card_number4 ==6){
      count6++;
    }
    else if (true_card_number4 ==7){
      count7++;
    }
    else if (true_card_number4 ==8){
      count8++;
    }
    else if (true_card_number4 ==9){
      count9++;
    }
    else if (true_card_number4 ==10){
      count10++;
    }
    
    //for card 5
    int rand_number5; //making the random number an integer
    int card_number5; //making the card number an integer
    int true_card_number5; //making the true card number an integer
    float suit5;
    rand_number5 = (int)(Math.random()*52+1); //assinging random numbers to be from 1-52
    card_number5= (rand_number5 +13) % 13 ; //assigning the card number range to be 13
    true_card_number5 = card_number5; //having the true card number equal the random card number that is drawn
    suit5 = rand_number5 / 13 ; //making sure that each suit only ranges between 13
   
    //System.out.println("The random number is " +rand_number); //prints out random number is and then what the random number generator generates
    
    if (suit5 == 0) {
      if (card_number5 == 1) {
        System.out.println(" the Ace of Diamonds"); //printing Ace when the random number is 1
        count1++;
      }
      else if (card_number5 == 11) {
       System.out.println(" the Jack of Diamonds "); //printing Jack when the random number is 11
        count11++;
      }
      else if (card_number5 == 12) {
       System.out.println(" the Queen of Diamonds "); //printing Queen when the random number is 12
        count12++;
      }
      else if (card_number5 == 0) {
       System.out.println(" the King of Diamonds "); //printing King when the random number is 13
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number5  + " of Diamonds "); //printing whatever the random number is (other than 1, 11, 12, and 13)
      }}
   
    else if (suit5 == 1) { //making sure that all numbers below that will be between 14-26 are clubs a.k.a suit == 1
      if (card_number5 == 1) {
        System.out.println(" the Ace of Clubs"); //printing Ace when the random number is 14
        count1++;
      }
    else if (card_number5 == 11) {
       System.out.println(" the Jack of Clubs "); //printing Jack when the random number is 24
      count11++;
      }
      else if (card_number5 == 12) {
       System.out.println(" the Queen of Clubs "); //printing Queen when the random number is 25
        count12++;
      }
      else if (card_number5 == 0) {
       System.out.println(" the King of Clubs "); //printing King when the random number is 26
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number5  + " of Clubs "); //printing whatever the random number is (other than 14, 24, 25, and 26)
      }}
    
    else if (suit5 == 2) { //making sure that all numbers below that will be between 27-39 are hearts a.k.a suit == 2
      if (card_number5 == 1) {
        System.out.println(" the Ace of Hearts"); //printing Ace when the random number is 27
        count1++;
      }
    else if (card_number5 == 11) {
       System.out.println(" the Jack of Hearts "); //printing Jack when the random number is 37
      count11++;
      }
      else if (card_number5 == 12) {
       System.out.println(" the Queen of Hearts "); //printing Queen when the random number is 38
        count12++;
      }
      else if (card_number5 == 0) {
       System.out.println(" the King of Hearts "); //printing King when the random number is 39
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number5  + " of Hearts "); //printing whatever the random number is (other than 27. 37, 38, and 39)
      }}
    
    else if (suit5 == 3) { //making sure that all numbers below that will be between 40-52 are spades a.k.a suit ==3
     if (card_number5 == 1) {
        System.out.println(" the Ace of Spades"); //printing Ace when the random number is 40
       count1++;
      }
    else if (card_number5 == 11) {
       System.out.println(" the Jack of Spades "); //printing Jack when the random number is 50
      count11++;
      }
      else if (card_number5 == 12) {
       System.out.println(" the Queen of Spades "); //printing Queen when the random number is 51
        count12++;
      }
      else if (card_number5 == 0) {
       System.out.println(" the King of Spades "); //printing King when the random number is 52
        count0++;
      }
      else {
       System.out.println(" the " + true_card_number5  + " of Spades "); //printing whatever the random number is (other than 40, 50, 51, and 52)
      }}
      if (true_card_number5 == 2){ 
      count2++;
        //counts how many times these numbers occur in card 5
    }
    else if (true_card_number5 == 3){ 
      count3++;
    }
    else if (true_card_number5 == 4){
      count4++;
    }
    else if (true_card_number5 ==5){
      count5++;
    }
    else if (true_card_number5 ==6){
      count6++;
    }
    else if (true_card_number5 ==7){
      count7++;
    }
    else if (true_card_number5 ==8){
      count8++;
    }
    else if (true_card_number5 ==9){
      count9++;
    }
    else if (true_card_number5 ==10){
      count10++;
    }
    System.out.println(); //adds a space between the list of cards and whether there is a pair, three of a kind, or high card hand
    if (count0 > 2 || count1 > 2 || count2 > 2 || count3 > 2 || count4 > 2 || 
        count5 > 2 || count6 > 2 || count7 > 2 || count8 > 2 || count9 > 2 || 
        count10 > 2 || count11 > 2 || count12 > 2){ 
      System.out.println("You have a three of a kind!"); //prints you have a three of a kind when you have more than 2 of a kind
    }
    else if (count0 > 1 || count1 > 1 || count2 > 1 || count3 > 1 || count4 > 1 || 
        count5 > 1 || count6 > 1 || count7 > 1 || count8 > 1 || count9 > 1 || 
        count10 > 1 || count11 > 1 || count12 > 1){ 
      System.out.println("You have a pair!"); //prints you have a pair when there is more than one of a kind but less than 3 of a kind
    }
    else{ 
      System.out.println("You have a high card hand!"); //prints out that you have a high card hand if the other two conditions are not met
    }
    
    
    
    
  } //end of main method
} //end of class