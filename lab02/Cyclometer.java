//This program prints number of minutes for each trip
//Prints the number of counts for each trip
//Prints the distance of each trip in miles
//Prints the distance for the two trips combined
public class Cyclometer{
  //main method required for every Java program
  public static void main(String[] args){
    int secsTrip1=480; //Number of seconds for trip 1
    int secsTrip2=3220; //Number of seconds for trip 2
    int countsTrip1=1561; //Number of counts for trip 1
    int countsTrip2=9037; //Number of counts for trip 2
    
    double wheelDiameter=27.0; //The wheel diameter
    double PI=3.14159; //The definition of pi
    int feetPerMile=5280; //How many feet in a mile
    int inchesPerFoot=12; //How many inches in a foot
    int secondsPerMinute=60; //How many seconds are in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //Making the distance of trip 1, distance of trip 2, and total distance a double for more storage
    
    System.out.println ("Trip 1 took "+ (secsTrip1/secondsPerMinute) +" minutes and had "+ countsTrip1+" counts.");
    System.out.println ("Trip 2 took "+ (secsTrip2/secondsPerMinute) +" minutes and had "+ countsTrip2+" counts.");
    
    //run the calculations; store the values. Document your
    //calculation here. What are you calculating?
    //
    //
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    distanceTrip1=inchesPerFoot*feetPerMile;// Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out the output data.
    System.out.println ("Trip 1 was "+distanceTrip1+" miles");
    System.out.println ("Trip 2 was "+distanceTrip2+" miles");
    System.out.println ("The total distance was "+totalDistance+" miles");
    
  } //end of main method
  
} //end of class