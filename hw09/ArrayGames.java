//Noah Sutherland, 04/15/19, hw09
//this homework gives practice with manipulating arrays and writing methods that have array parameters
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class ArrayGames {
  public static int[] generate(){ //a method called generate
    Random randNumgen=new Random(); //initializes randNumgen
    int z=randNumgen.nextInt(10)+10; //makes the integer z equal to a random integer between 10-20
    int[] Array=new int[z];
    for (int i=0; i<z; i++){ //for loop that has i equal to 0 and runs when i is less than z and increments i by 1
      Array[i]=randNumgen.nextInt(20);
    }
    print(Array); //prints Array
    return Array;
  }

  public static void print(int[] Array){ //a method called print
    int z=Array.length; //makes the integer z equal to the length of Array
    for (int i=0; i<z; i++){ //for loop that makes i equal to 0, runs when i is less than z, and increments i by 1
      System.out.println(Array[i]);
    }  }

  public static int[] insert(int[] Array1, int[] Array2){ //a method called insert
    Random randNumgen=new Random(); //initializes randNumgen
    int Endlength=Array1.length+Array2.length; //initializes Endlength and makes it equal to the length of Array1 plus the length of Array2
    int Num=randNumgen.nextInt(Array1.length); //initializes Num and makes it equal to the random number that is up to the Array1 length
    int[] FinalArray=new int[Endlength]; //makes the Array called FinalArray equal to the the end length
    System.out.println("New insert method output: "); //prints out that this is the new insert method output
    for (int i=0; i<Num; i++){ //for loop that has i equal to 0 and runs when i is less than Num and increments i by 1
      FinalArray[i]=Array1[i];
    }
    int z=0; //initializing z as an integer equal to 0
    for (int i=Num; i<(Num+Array2.length); i++){ //for loop that has i equal Num and runs when i is less than Num added to the Array2 length and increments i by 1
      FinalArray[i]=Array2[z]; //makes the final array of i equal to Array2 with z
      z++; //increments z by 1 every time the for loop runs
    }
    for (int i=Num+Array2.length; i<Endlength; i++){ //has int i equal to Num plus Array2 length, and runs when i is less than the Endlength, and increments i by 1
      FinalArray[i]=Array1[i-Array2.length];
    }
    return FinalArray;
  }

  public static int[] shorten(int[] Array, int randNumber){ //method called shorten
    if (randNumber>=Array.length){ //if statement for if the random number is greater than or equal to the array length inputted in the method
      System.out.println("The array was not shortened because the entry "+ randNumber +" was not in the array."); //prints out that the array wasn't shortened
      return Array; //exits the method because the array won't need to be shortened
    }
    else { //else statement for if the code will actually shorten the array
      int[] FinalArray=new int[Array.length-1];
      for (int i=0; i<randNumber; i++){ //for loop that has i equal to 0 and runs when i is less than random number and increments i by 1
        FinalArray[i]=Array[i]; //makes the final array with input i equal to array with input i
      }
      for (int i=randNumber; i<FinalArray.length; i++){ //for loop that has i equal to random number and runs when i is less than the final array length and increments i by 1
        FinalArray[i]=Array[i+1]; //makes final array with input i equal to array with input i plus 1
      }
      System.out.println("Your shortened Array is: "); //prints out "Your shortened Array is: "
      return FinalArray;
    }  }

  public static void main (String[] args){ //the main method
    Scanner myScanner=new Scanner(System.in); //initializing the Scanner as myScanner
    Random randNumgen=new Random(); //initializing the Random as randNumgen
    int i=0; //initializing i as an integer equal to 0
    boolean Ask=false; //initializing Ask as a boolean that is false
    while (Ask==false){  //while loop that runs when Ask is equal to false to make sure the user enters Insert or Shorten
      System.out.println("Please enter whether you want to [Insert] or [Shorten]?"); //asks the user if they want to insert or shorten
      String userInput=myScanner.next(); //makes userInput equal to the next thing the user inputs
      if (userInput.equals("Insert")) { //if statement for if the userInput equals Insert
        i=1; //sets int i equal to 1 for the next set of if statements
        Ask=true; //sets Ask equal to true so it will end up exiting the while loop
      }
      if (userInput.equals("insert")) { //if statement for if the userInput equals insert
        i=1; //sets int i equal to 1 for the next set of if statements
        Ask=true; //sets Ask equal to true so it will end up exiting the while loop
      }
      else if (userInput.equals("Shorten")) { //else if statement for when userInput equals Shorten
        i=2; //sets i equal to 2 for the next set of if statements
        Ask=true; //sets Ask equal to true to exit out of the while loop
      }
      else if (userInput.equals("shorten")) { //else if statement for when userInput equals shorten
        i=2; //sets i equal to 2 for the next set of if statements
        Ask=true; //sets Ask equal to true to exit out of the while loop
      }
      else { //else statement for if the user did not enter either of the options above
        System.out.println("Please input the word [Insert] or [Shorten]."); //asks the user to make sure they enter either insert or shorten
      }    }
    if (i==1) { //if statement for when i is equal to 1 which is when the user inputs insert
      System.out.println("Array 1 is: "); //will output "Array 1 is: "
      int[] Array1=generate(); //initializes the single Array Array1
      System.out.println("Array 2 is: "); //prints "Array 2 is: "
      int[] Array2=generate(); //initializes the single Array Array2
      int[] FinalArray=insert(Array1, Array2); //makes the final Array equal to the insert of Array1 and Array2
      System.out.println("The Final Array is: "); //prints "The Final Array is: "
      print(FinalArray); //will print the FinalArray
    }
    if (i==2) { //if statement for if the integer i is equal to 2
      int[] Array=generate(); //initializes the single array Array
      int randNumber=randNumgen.nextInt(20); //setting randNumber equal to whatever the random number is generated by our random number generator
      int[] FinalArray=shorten(Array, randNumber);
      print(FinalArray); //will print the FinalArray
    }
  }
}