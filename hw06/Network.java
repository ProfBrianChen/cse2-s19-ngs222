//Noah Sutherland, hw06, 03/19/18, this homework provides practice with do-while and while loops through prompting the user to input four integers and outputting the correct number and order of characters
import java.util.Scanner; //imports a scanner so that the user can input information
public class Network{
  public static void main (String[]args) {
    int height = 0; //initializes the height and makes it equal to 0
    int width = 0; //initializes width and makes it equal to 0
    int size = 0; //initializes the size of the square and makes it equal to 0
    int length = 0; //initializes edge length and makes it equal to 0
    int count = 0; //initializes count and makes it equal to 0
    int count1 = 0; //initializes a new counter and makes it equal to 0
    Scanner myScanner = new Scanner(System.in); //assigning the scanner to myScanner so the user can input information
    
    //for the height
    System.out.print ("Enter an integer for the height: "); //asks the user for an integer that represents height
    while (!myScanner.hasNextInt() || height<=0) {
       if (myScanner.hasNextInt()){ 
        height= myScanner.nextInt(); 
            if (height>0){ 
              break; //breaks the loop when the user inputs a positive integer and goes on to the next question
            } }
      else { 
        String junkWord = myScanner.next(); //disregards what the user input and asks again
      }
      System.out.print ("Error: Please enter a positive integer that represents height: "); //asks the user to enter an integer that represents height after they entered something incorrectly
    }
    
    //for the width
     System.out.print ("Enter an integer for the width: "); //asks the user for an integer that represents width
    while (!myScanner.hasNextInt() || width<=0) { 
       if (myScanner.hasNextInt()){ 
        width= myScanner.nextInt(); 
            if (width>0){ 
              break; //brekas the loop when the user inputs a positive integer and goes on to the next question
            }  }
      else { 
        String junkWord = myScanner.next(); //disregards what the user input and asks again
      }
      System.out.print ("Error: Please enter a positive integer that represents width: "); //asks the user to enter an integer that represents width after they entered something incorrectly
    }
    
    //for the square size
   System.out.print ("Enter an integer for the square size: "); //asks the user for an integer that represents the square size
    while (!myScanner.hasNextInt() || size<=0) { 
       if (myScanner.hasNextInt()){ 
        size= myScanner.nextInt(); 
            if (size>0){ 
              break; //breaks the loop when the user inputs a positive integer and goes on to the next question
            }  }
      else { 
        String junkWord = myScanner.next(); //disregards what the user input and asks again
      }
      System.out.print ("Error: Please enter a positive integer that represents square size: "); //aks the user to enter an integer that represents the square size after they entered something incorrectly
    }
    
    //for edge length
   System.out.print ("Enter an integer for the edge length: "); //asks the user for an integer that represents the edge length
    while (!myScanner.hasNextInt() || length<=0) { 
       if (myScanner.hasNextInt()){ 
        length= myScanner.nextInt(); 
            if (length>0){ 
              break; //breaks the loop when the user inputs a positive integer and goes on to the next question
            }  }
      else { 
        String junkWord = myScanner.next(); //disregards what the user input and asks again
      }
      System.out.print ("Error: Please enter a positive integer that represents the edge length: "); //asks the user to enter an integer that represents the edge length after they entered something incorrectly
    }

    //for the actual output after the user inputs the numbers they want
  if (size%2==0){ //an if statement for when the square size is an even number
    for(int i= 0; i < height; i++){ 
        if (i%(size-1) == 0){ 
           for(int k= 0; k < ((width)/(size+length)); k++){ 
             System.out.print ("#"); //will output #
             for (int l = 0; l<size-2; l++){ 
                System.out.print ("-"); //will output -
             } 
             System.out.print ("#"); //will output #
             for (int j=0; j<length; j++){ 
               System.out.print (" "); //will output a space
             }   }
          
          if (width%(size+height) != 0){ //if statement for when there is a remainder 
            System.out.print ("#"); //will output #
            for (int l=1; l<(width%(size+length)); l++){ 
              System.out.print("-"); //will output -
            }   }
          
          count1++; //increases the second counter created by one which then counts the number of vertical characters
          System.out.println (""); //outputs a new line before the next information
          for(int k= 0; k < ((width)/(size+length)); k++){
             System.out.print ("#"); //will output #
             for (int l = 0; l<size-2; l++){ 
                System.out.print ("-"); //will ouput -
             } 
             System.out.print ("#"); //will output #
             for (int j=0; j<length; j++){ 
               System.out.print (" "); //will output a space
             }   }
          
          if (width%(size+height) != 0){ //an if statement if there ends up being a remainder
            System.out.print ("#"); //will output #
            for (int l=1; l<(width%(size+length)); l++){ 
              System.out.print("-"); //will output-
            }   }  }
      
      else{ //an else statement if the size doesn't go into the height exactly how the program wants it
          for (int m=0; m < ((width)/(size+length)); m++){ 
            System.out.print ("|"); //will output |
            if(count1 % (size/2) == 0 && count1 % size != 0){ 
             for (int b= 0; b<size-2; b++){ 
               System.out.print (" "); //will output a space
            }
              System.out.print("|"); //will output |
              for (int b= 0; b<length; b++){ 
               System.out.print ("-"); //will output -
            }    }
            
            else{ //an else statement for when the vertical line starts at the original position and isn't half the size 
            for (int b= 0; b<size-2; b++){ 
               System.out.print (" "); //will output a space
            }
              System.out.print ("|"); //will output |
              for (int f=0; f<length; f++){ 
                System.out.print (" "); //will output a space
              }    }      }
        
            if (width%(size+height) != 0){ // an if statement for when there is a remainder
            System.out.print ("|"); //will output |
            }    }
          count1++; //increases the second counter made by one to make a new row in the output 
          System.out.println (""); //will output a new line
        }   }
    
    else{ //an else statement for when the square size is a odd number 
      for(int i= 0; i < height; i++){ 
        if (i%(length+1) ==0){ //an if statement for when there is no remainder
           for(int k= 0; k < (width/(length+1)); k++){ 
             System.out.print ("#"); //will output #
             for (int l = 0; l<length; l++){ 
                System.out.print ("-"); //will output -
             }       }
          
          if (width%(length+1) != 0){ //an if statement for when there is a remainder
            System.out.print ("#"); //will output #
            for (int l=1; l<(width%(length+1)); l++){ 
              System.out.print("-"); //will output -
            } }
          
          System.out.println (""); //will output a new line
        }
        else{ //an else statement for when there need to be partials 
          count= (width/(length+1));
          if (width%(length+1) != 0){ //an if statement for when there is a remainder
            count++; //increases the original count by 1
          }
          for (int m=0; m<count; m++){
            System.out.print ("|"); //will output |
            for (int b= 0; b<length; b++){ 
               System.out.print (" "); //will output a space
            }   }
          
          System.out.println (""); //will output a new line
        }   }  }
      
      } //end of main method
} //end of public class