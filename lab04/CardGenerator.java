// A random card generator for me... a magician to practice my magic tricks with the output of a card with the card number and the suit
//diamonds=1-13
//clubs=14-26
//hearts=27-39
//spades=40-52
import java.lang.Math;
public class CardGenerator{
  //main method required for every Java program
  public static void main(String[] args){
    int rand_number; //making the random number an integer
    int card_number; //making the card number an integer
    int true_card_number; //making the true card number an integer
    float suit;
    rand_number = (int)(Math.random()*52+1); //assinging random numbers to be from 1-52
    card_number = (rand_number+13) % 13 ; //assigning the card number range to be 13
    true_card_number = card_number; //having the true card number equal the random card number that is drawn
    suit = rand_number / 13 ; //making sure that each suit only ranges between 13
   
    System.out.println("The random number is " +rand_number); //prints out random number is and then what the random number generator generates
    
    
    if (suit == 0) {
      if (card_number == 1) {
        System.out.println("You picked the Ace of Diamonds"); //printing Ace when the random number is 1
      }
      else if (card_number == 11) {
       System.out.println("You picked the Jack of Diamonds "); //printing Jack when the random number is 11
      }
      else if (card_number == 12) {
       System.out.println("You picked the Queen of Diamonds "); //printing Queen when the random number is 12
      }
      else if (card_number == 0) {
       System.out.println("You picked the King of Diamonds "); //printing King when the random number is 13
      }
      else {
       System.out.println("You picked the " + true_card_number  + " of Diamonds "); //printing whatever the random number is (other than 1, 11, 12, and 13)
      }}
   
    else if (suit == 1) { //making sure that all numbers below that will be between 14-26 are clubs a.k.a suit == 1
      if (card_number == 1) {
        System.out.println("You picked the Ace of Clubs"); //printing Ace when the random number is 14
      }
    else if (card_number == 11) {
       System.out.println("You picked the Jack of Clubs "); //printing Jack when the random number is 24
      }
      else if (card_number == 12) {
       System.out.println("You picked the Queen of Clubs "); //printing Queen when the random number is 25
      }
      else if (card_number == 0) {
       System.out.println("You picked the King of Clubs "); //printing King when the random number is 26
      }
      else {
       System.out.println("You picked the " + true_card_number  + " of Clubs "); //printing whatever the random number is (other than 14, 24, 25, and 26)
      }}
    
    else if (suit == 2) { //making sure that all numbers below that will be between 27-39 are hearts a.k.a suit == 2
      if (card_number == 1) {
        System.out.println("You picked the Ace of Hearts"); //printing Ace when the random number is 27
      }
    else if (card_number == 11) {
       System.out.println("You picked the Jack of Hearts "); //printing Jack when the random number is 37
      }
      else if (card_number == 12) {
       System.out.println("You picked the Queen of Hearts "); //printing Queen when the random number is 38
      }
      else if (card_number == 0) {
       System.out.println("You picked the King of Hearts "); //printing King when the random number is 39
      }
      else {
       System.out.println("You picked the " + true_card_number  + " of Hearts "); //printing whatever the random number is (other than 27. 37, 38, and 39)
      }}
    
    else if (suit == 3) { //making sure that all numbers below that will be between 40-52 are spades a.k.a suit ==3
     if (card_number == 1) {
        System.out.println("You picked the Ace of Spades"); //printing Ace when the random number is 40
      }
    else if (card_number == 11) {
       System.out.println("You picked the Jack of Spades "); //printing Jack when the random number is 50
      }
      else if (card_number == 12) {
       System.out.println("You picked the Queen of Spades "); //printing Queen when the random number is 51
      }
      else if (card_number == 0) {
       System.out.println("You picked the King of Spades "); //printing King when the random number is 52
      }
      else {
       System.out.println("You picked the " + true_card_number  + " of Spades "); //printing whatever the random number is (other than 40, 50, 51, and 52)
      }}
  } //end of main method
  } //end of class