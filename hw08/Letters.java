//04/07/19, Noah Sutherland homework Part 1, this homework gives us practice in writing methods that use Arrays
//This program is supposed to create three random outputs, the first one with random upper and lower case letters from A to Z
//The second goes from A to M with upper and lower case numbers
//The third goes from N to Z with upper and lower case numbers
import java.util.Arrays;
public class Letters{
public static void main(String[]args){
		int size=10; //initializing size as an integer equal to 10 for the number of letters
		char[] CharArray=new char[size];
		System.out.print("Random character array: "); //prints out "Random character array: " without entering a line
		for(int i = 0; i < CharArray.length; i++){ //for loop for the characters
			double RandLetter=Math.random()*2; //initializing Random Letter as a double random number multiplied by 2
			if(RandLetter>1){ //if statement for if the Random Letter is greater than 1
				CharArray[i]=(char)((int)(Math.random()*25)+98);
			}
			else if(RandLetter<=1){ //else if statement for if the Random Letter is less than or equal to 1
				CharArray[i]=(char)((int)(Math.random()*25)+66);
			}
			System.out.print(CharArray[i]); //prints whatever the new character is without a space
		}
		System.out.println(); //enters a space before the next output
		getAtoM(CharArray); //goes to the method called "getAtoM"
		getNtoZ(CharArray); //goes to the method called "GetNtoZ"
	}

//new method for getAtom
	public static char[] getAtoM(char[] list){
		Arrays.sort(list);
		char[] atom=new char[list.length];
		int counter=0; //initializes counter as an integer equal to 0
		for(int i=0; i<list.length; i++){ //for loop that makes i equal to, and only runs when i is less than the list.length, and increments i by one at the end
			if(list[i]>='A' & list[i]<='M' || list[i]>='a' & list[i]<='m'){ //if statement that makes sure the characters are between A and M for both lower case and upper case letters
				atom[i]=list[i];
			}
			else{ //else statement if the letter is not between A and M
				atom[i]=' '; //makes atom equal to a space so it does not include the letter that is not in the range
				counter++; //increments the counter by 1
			}	}
		Arrays.sort(atom);
		char[] AtoMList=new char[list.length-counter];
		System.out.print("AtoM characters: "); //prints out "AtoM characters: " without a space after it
		for(int i=counter; i<atom.length; i++){ //for loop that makes the integer i equal to the counter, when i is less than the atom length and increments i by 1 at the end
			AtoMList[i-counter]=atom[i];
			System.out.print(AtoMList[i-counter]); //prints out the AtoMList[i-counter] which is now equal to atom[i]
		}
		System.out.println(); //enters the space after the last ouput
		return AtoMList; //spits out AtomList
	}

//new method for getNtoZ
	public static char[] getNtoZ(char[] list){
		Arrays.sort(list);
		char[] ntoz=new char[list.length];
		int counter=0; //initializing the counter as an integer equal to 0
		for(int i=0; i<list.length; i++){ //for loop that makes the integer i equal to 0 and when i is less than the list.length, the loop will run and at the end increment i by 1
			if(list[i]>='N' & list[i]<='Z' || list[i]>='n' & list[i]<='z'){ //if statement that makes sure the characters are between N and Z for both lower case and upper case letters
				ntoz[i]=list[i];
			}
			else{ //else statement if the letter is not between N and Z
				ntoz[i]=' '; //makes ntoz equal to a space so it does not include the letter that is not in the range
				counter++; //increments the counter by 1
			}	}
		Arrays.sort(ntoz);
		char[] NtoZList=new char[list.length-counter];
		System.out.print("NtoZ characters: "); //prints out "NtoZ characters: " without a space to the next line
		for(int i=counter; i<ntoz.length; i++){ //for loop that makes i equal to counter and runs when i is less than ntoz.length and ends up incrementing i by 1
			NtoZList[i-counter]=ntoz[i];
			System.out.print(NtoZList[i-counter]); //prints NtoZList[i-counter] which is now equal to ntoz[i] based on the previous line
		}
		System.out.println(); //prints out an enter
		return NtoZList; //spits out NtoZList
	}
} //end of public class
