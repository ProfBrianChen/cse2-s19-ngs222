//04/07/19, Noah Sutherland homework Part 2, this homework gives us practice in writing methods that use Arrays
//This program will be used to help the user play the lottery
//This program will ask the user for integers between 0 and 59 and if they are incorrect it will say they lose and if it is correct will say they win
import java.util.Arrays;
import java.util.Scanner;
public class PlayLottery{
public static void main(String[] args){
  Scanner UserInput=new Scanner(System.in); //making the Scanner named UserInput
  System.out.print("Enter 5 numbers between 0 and 59: "); //prints out "Enter 5 numbers between 0 and 59: " without creating a new line
  int[] ArrayNumber=new int[5]; //first array used
  String UserNum=UserInput.nextLine();
  String[] Seperation=UserNum.split(","); //second array used
  for(int i=0; i<Seperation.length; i++){ //for loop that makes i equal to the integer 0 and runs when i is less than the Seperation.length, and incrememnts i by 1
    ArrayNumber[i]=Integer.parseInt(Seperation[i]);
  } //end of loop
int[] ArrayRandom=new int[5];
    ArrayRandom=numbersPicked(); //setting the ArrayRandom equal to the selected number
    System.out.print("The winning numbers are: "); //prints out "The winning numbers are: " without entering the line
  for(int z=0; z<4; z++){ //for loop that makes z equal to the integer 0 and runs when z is less than 4 and increments z by 1
    System.out.print(ArrayRandom[z] + ", "); //prints out the ArrayRandom[z] with a comma and a space after it
  }
    System.out.println(ArrayRandom[4] + " ");
  boolean WonLottery=userWins(ArrayNumber, ArrayRandom); //assigning WonLottery as a boolean that will either be true or false to the new method userwins and its output
  if(WonLottery){ //if statement for if the user won
    System.out.println("You win"); //will print out "You win" if the program runs and the user selected all of the correct numbers
  }
  else{ //else statement for if the user lost
    System.out.println("You lose"); //will print out "You lose" if the program runs and the user did not select all of the correct numbers
  }
} //end of main method

//new method that will calculate whether the user input the correct numbers
public static boolean userWins(int[] user, int[] winning){
  for(int i=0; i<5; i++){ //for loop that makes i equal to integer 0 and runs when i is less than 5 and increments i by 1
    if(user[i]!=winning[i]){ //if the user input does not equal the winning input than the program will return false
      return false; //returns false
    }  }
  return true; //returns true if the user does equal the winning input
}

//new method for the numbers picked
public static int[] numbersPicked(){
  int[] ArrayRandom=new int[5];
  for(int i=0; i<5; i++){ //for loop that makes i equal to integer 0 and runs when i is less than 5 and increments i by 1
    for(int z=0; z<i; z++){ //nested for loop that makes z equal to integer 0 and runs when z is less than i and increments z by 1
      while(true){ //nested while loop for when the boolean is true
    ArrayRandom[i]=(int)(Math.random()*60);
        if(ArrayRandom[z]!=ArrayRandom[i]){ //if statement for when the ArrayRandom[z] is not equal to ArrayRandom[i]
          break; //breaks out of the statement
        } }   } }
  return ArrayRandom; //returns ArrayRandom
}
} //end of public class
